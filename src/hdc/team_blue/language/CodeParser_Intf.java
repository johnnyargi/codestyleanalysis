package hdc.team_blue.language;

import java.nio.file.Path;

public interface CodeParser_Intf {
	public boolean parseFile(Path file);    //the code to parse a code file. Returns false on error or problem
	public void printStatistics();          //this is used to print any statistics kept for a file
}
