package hdc.team_blue.language.html;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hdc.team_blue.language.CodeParser_Intf;

public class HTML2_Analyzer implements CodeParser_Intf {
	private static final int MAX_FILE_BYTES = 1048576;		//limit how large will the file be
	private Path currFile;                                  //the current file
	//Patterns to search for
	private static final Pattern doctypePtrn = Pattern.compile("(^<!DOCTYPE [^<>]*?>)");
	private static final Pattern headPtrn = Pattern.compile("(?i:(<HEAD>.*?</HEAD>))",  Pattern.DOTALL);
	private static final Pattern titlePtrn = Pattern.compile("(?i:(<TITLE>.*?</TITLE>))", Pattern.DOTALL);
	private static final Pattern metaPtrn = Pattern.compile("(?i:(<META\\s.*?>))", Pattern.DOTALL);
	private static final Pattern tagPtrn = Pattern.compile("</?([^<!]+?)>");
	private static final Pattern attributePtrn = Pattern.compile("(\\s([A-Za-z]+[0-9]*)(\\s*=\\s*)\"(.+?)\")", Pattern.DOTALL);
	private static final Pattern commentPtrn = Pattern.compile("(<!--(.+?)-->)", Pattern.DOTALL);
	//statistics variables ===========
	//general
	private int numberOfLines;
	private int numberOfEmptyLines;
	//tag related
	private boolean doctypeUsed;
	private boolean headUsed;
	private boolean titleUsed;
	private boolean metaUsed;
	private int numberOfTags;
	private int uppercaseTags;
	private int lowercaseTags;
	private int mixedcaseTags;
	private int numberOfAttributes;
	private int uppercaseAttributes;
	private int lowercaseAttributes;
	private int mixedcaseAttributes;
	//comment related
	private int numberOfCommentBlocks;
	private int totalLinesOfComments;
	//Indentation
	private boolean hasIndentation;
	
	public HTML2_Analyzer() {
		numberOfLines = 0;
		numberOfEmptyLines = 0;
		headUsed = false;
		doctypeUsed = false;
		titleUsed = false;
		metaUsed = false;
		numberOfTags = 0;
		uppercaseTags = 0;
		lowercaseTags = 0;
		mixedcaseTags = 0;
		numberOfAttributes = 0;
		uppercaseAttributes = 0;
		lowercaseAttributes = 0;
		mixedcaseAttributes = 0;
		numberOfCommentBlocks = 0;
		totalLinesOfComments = 0;
		hasIndentation = false;
	}
	
	/* 
	 * Will read the file and send the code to an analysis function. Since we use regular expressions
	 * the whole text is needed in memory, so the whole file will be loaded as once. Splitting it to
	 * blocks would make detecting patterns harder.
	 */
	@Override
	public boolean parseFile(Path file) {
		try {
			if(Files.size(file) > MAX_FILE_BYTES) {
				System.out.println("File too large to read.");
				return false;
			}
		} catch (IOException d) {
			System.err.println("Error in reading file : " + d.getMessage());
			return false;
		}
		//Open the file and process line by line
		try(BufferedReader fileStream = new BufferedReader(new InputStreamReader( new FileInputStream(file.toString()), "UTF-8"))) {
			currFile = file;
			char [] buffer = new char[MAX_FILE_BYTES];  //the buffer that will keep the text read so far
			String codeText = "";                       //this will have the current buffer into a string
			fileStream.read(buffer);                    //read all at once
			codeText = String.valueOf(buffer);
			codeAnalysis(codeText);
		}catch(IOException e) {
			System.err.println("Error in reading file : " + e.getMessage());
		}
		return true;
	}
	
	/*
	 * This function will analyze the text and search for patterns.
	 * It uses the current code plus the previous last line read 
	 */
	private void codeAnalysis(String code) {
		//First do a minimum analysis line by line
		lineChecker(code);
		//Then use regulars expressions
		searchForCertainElements(code);
		//Second pass - Check style inside tags
		detectTagsAndAttributes(code);
		//Detect comments
		detectComments(code);
	}

	//This function will be used to count every line and check for indentation
	private void lineChecker(String code) {
		boolean changedLine = false;
		boolean possibleIndentation = false;
		boolean possibleTag = false;
		for(int i=0; i < code.length(); i++) {
			if(code.charAt(i) == '\n') {    //found a new line
				numberOfLines ++;
				if(changedLine) {
					numberOfEmptyLines ++;
				}else {
					changedLine = true;
					possibleIndentation = false;
				}
			}else {
				if(changedLine) {
					changedLine = false;
					//now that we changed line, let's check for possible indentation
					if(!possibleIndentation) {
						if((code.charAt(i) == ' ') || (code.charAt(i) == '\t')) {
							possibleIndentation = true;
						}
					}
				}else {
					if(possibleIndentation) {
						if((code.charAt(i) == '<')) {    //we might have a tag declaration
							possibleTag = true;
						}else {
							if(possibleTag) {    //avoid catching comments
								if(code.charAt(i) == '!') {
									possibleTag = false;
									possibleIndentation = false;
								}else {
									hasIndentation = true;
									possibleTag = false;
									possibleIndentation = false;
								}
							}else {
								if((code.charAt(i) != ' ') && (code.charAt(i) != '\t')) {
									possibleIndentation = false;
									possibleTag = false;
								}
							}	
						}
					}
				}
			}
		}
	}

	//Searches for the existence of certain elements
	private void searchForCertainElements(String code) {
		Matcher elementMatcher = doctypePtrn.matcher(code);
		if(elementMatcher.find()) {
			//System.out.println("Doctype declaration detected : " + patternMatcher.group(1));
			doctypeUsed = true;
		}
		//Search for the HEAD tag
		elementMatcher.reset();
		elementMatcher.usePattern(headPtrn);
		if(elementMatcher.find()) {
			headUsed = true;
			//System.out.println("HEAD declaration detected : " + patternMatcher.group(1));
			//Search for TITLE
			elementMatcher.reset(elementMatcher.group(1));
			elementMatcher.usePattern(titlePtrn);
			if(elementMatcher.find()) {
				//System.out.println("TITLE declaration detected : " + patternMatcher.group(1));
				titleUsed = true;
			}
			//Search for META
			elementMatcher.reset();
			elementMatcher.usePattern(metaPtrn);
			while(elementMatcher.find()) {
				//System.out.println("META declaration detected : " + patternMatcher.group(1));
				metaUsed = true;
			}
		}	
	}
	
	//Used to detect and analyze tags and their attributes
	private void detectTagsAndAttributes(String code) {
		Matcher patternMatcher = tagPtrn.matcher(code);
		String tagText = "";
		String [] tokens;
		Matcher tagTextMatcher;
		//For each tag check the style
		while(patternMatcher.find()) {
			//System.out.println("Tag detected : " + patternMatcher.group(1));
			numberOfTags ++;
			//get the name of the tag
			tagText = patternMatcher.group(1);
			if(tagText.equals("")) {
				continue;
			}
			tokens = tagText.split(" |\n", 10);
			//System.out.println("Tag name : "+tokens[0]);
			//Check for tag name case
			if(tokens[0].equals(tokens[0].toUpperCase())) {
				 uppercaseTags ++;
			}else if(tokens[0].equals(tokens[0].toLowerCase())) {
				lowercaseTags ++;
			}else {
				mixedcaseTags ++;
				//System.out.println("Mixed case tag "+ tokens[0] +" in file : "+ currFile);
			}
			//search for the attributes
			if(tokens.length > 1) {
				tagText = tagText.substring(tokens[0].length());
				tagTextMatcher = attributePtrn.matcher(tagText);
				while(tagTextMatcher.find()) {
					numberOfAttributes ++;
					String attributeName = tagTextMatcher.group(2);
					//System.out.println("Attribute : "+ attributeName);
					//Check for attribute name case
					if(attributeName.equals(attributeName.toUpperCase())) {
						 uppercaseAttributes ++;
					}else if(attributeName.equals(attributeName.toLowerCase())) {
						lowercaseAttributes ++;
					}else {
						mixedcaseAttributes ++;
						//System.out.println("Mixed case attribute " + attributeName + " in file : "+ currFile);
					}
				}
			}
			//System.out.println("*******");
		}
	}
	
	//Detect comments and their number of lines
	public void detectComments(String code) {
		Matcher patternMatcher = commentPtrn.matcher(code);
		while(patternMatcher.find()) {
			numberOfCommentBlocks ++;
			totalLinesOfComments ++;
			//System.out.println("Comment : " + patternMatcher.group(1));
			String commentText = patternMatcher.group(2);
			for(int i=0; i < commentText.length(); i++) {
				if(commentText.charAt(i) == '\n') {
					totalLinesOfComments ++;
				}
			}
		}
	}
	
	//It will print the statistics gathered
	@Override
	public void printStatistics() {
		StringBuilder statistics = new StringBuilder("Analysis of given file : \n");
		statistics.append("Total number of lines : " + numberOfLines + "\n");
		statistics.append("Number of empty lines : " + numberOfEmptyLines + "\n");
		statistics.append("Has identation : " + hasIndentation + "\n");
		statistics.append("DOCTYPE declaration used : " + doctypeUsed + "\n");
		statistics.append("HEAD tag used : " + headUsed + "\n");
		statistics.append("TITLE element used : " + titleUsed + "\n");
		statistics.append("META element used : " + metaUsed + "\n");
		statistics.append("Total number of tags (open and closed) : " + numberOfTags + "\n");
		statistics.append("Number of uppercase tags (open and closed) : " + uppercaseTags + "\n");
		statistics.append("Number of lowercase tags (open and closed) : " + lowercaseTags + "\n");
		statistics.append("Number of mixed-case tags (open and closed) : " + mixedcaseTags + "\n");
		statistics.append("Total number of attributes : " + numberOfAttributes + "\n");
		statistics.append("Number of uppercase attributes : " + uppercaseAttributes + "\n");
		statistics.append("Number of lowercase attributes : " + lowercaseAttributes + "\n");
		statistics.append("Number of mixed-case attributes : " + mixedcaseAttributes + "\n");
		statistics.append("Number of comments (one line or blocks) : " + numberOfCommentBlocks + "\n");
		statistics.append("Number of comment lines : " + totalLinesOfComments + "\n");
		System.out.println(statistics);
	}

	//Getters - Setters =========================================================================

	public int getNumberOfLines() {
		return numberOfLines;
	}

	public int getNumberOfEmptyLines() {
		return numberOfEmptyLines;
	}

	public boolean isDoctypeUsed() {
		return doctypeUsed;
	}

	public boolean isHeadUsed() {
		return headUsed;
	}

	public boolean isTitleUsed() {
		return titleUsed;
	}

	public boolean isMetaUsed() {
		return metaUsed;
	}

	public int getNumberOfTags() {
		return numberOfTags;
	}

	public int getUppercaseTags() {
		return uppercaseTags;
	}

	public int getLowercaseTags() {
		return lowercaseTags;
	}

	public int getMixedcaseTags() {
		return mixedcaseTags;
	}

	public int getNumberOfAttributes() {
		return numberOfAttributes;
	}

	public int getUppercaseAttributes() {
		return uppercaseAttributes;
	}

	public int getLowercaseAttributes() {
		return lowercaseAttributes;
	}

	public int getMixedcaseAttributes() {
		return mixedcaseAttributes;
	}

	public int getNumberOfCommentBlocks() {
		return numberOfCommentBlocks;
	}

	public int getTotalLinesOfComments() {
		return totalLinesOfComments;
	}

	public boolean hasIndentation() {
		return hasIndentation;
	}
}
