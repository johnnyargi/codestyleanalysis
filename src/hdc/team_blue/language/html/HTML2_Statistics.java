/*
 * This class will be used to define statistics used
 * when parsing multiple html files
 */
package hdc.team_blue.language.html;

public class HTML2_Statistics {
	//general
	private int numberOfFiles;
	private int totalNumberOfLines;
	private int totalNumberOfEmptyLines;
	//tag related
	private int filesWithDoctypeElements;
	private int filesWithHeadElements;
	private int filesWithTitleElements;
	private int filesWithMetaElements;
	private int totalNumberOfTags;
	private int totalUppercaseTags;
	private int totalLowercaseTags;
	private int totalMixedcaseTags;
	private int totalNumberOfAttributes;
	private int totalUppercaseAttributes;
	private int totalLowercaseAttributes;
	private int totalMixedcaseAttributes;
	//comment related
	private int totalNumberOfCommentBlocks;
	private int totalCommentLines;
	//Indentation
	private int filesWithIndentation;
	
	public HTML2_Statistics() {
		numberOfFiles = 0;
		totalNumberOfLines = 0;
		totalNumberOfEmptyLines = 0;
		filesWithDoctypeElements = 0;
		filesWithHeadElements = 0;
		filesWithTitleElements = 0;
		filesWithMetaElements = 0;
		totalNumberOfTags = 0;
		totalUppercaseTags = 0;
		totalLowercaseTags = 0;
		totalMixedcaseTags = 0;
		totalNumberOfAttributes = 0;
		totalUppercaseAttributes = 0;
		totalLowercaseAttributes = 0;
		totalMixedcaseAttributes = 0;
		totalNumberOfCommentBlocks = 0;
		totalCommentLines = 0;
		filesWithIndentation = 0;	
	}
	
	public void updateStatistics(HTML2_Analyzer analyzer) {
		numberOfFiles ++;
		totalNumberOfLines += analyzer.getNumberOfLines();
		totalNumberOfEmptyLines += analyzer.getNumberOfEmptyLines();
		if(analyzer.isDoctypeUsed()) {
			filesWithDoctypeElements ++;
		}
		if(analyzer.isHeadUsed()) {
			filesWithHeadElements ++;
		}
		if(analyzer.isTitleUsed()) {
			filesWithTitleElements ++;
		}
		if(analyzer.isMetaUsed()) {
			filesWithMetaElements ++;
		}
		totalNumberOfTags += analyzer.getNumberOfTags();
		totalUppercaseTags += analyzer.getUppercaseTags();
		totalLowercaseTags += analyzer.getLowercaseTags();
		totalMixedcaseTags += analyzer.getMixedcaseTags();
		totalNumberOfAttributes += analyzer.getNumberOfAttributes();
		totalUppercaseAttributes += analyzer.getUppercaseAttributes();
		totalLowercaseAttributes += analyzer.getLowercaseAttributes();
		totalMixedcaseAttributes += analyzer.getMixedcaseAttributes();
		totalNumberOfCommentBlocks += analyzer.getNumberOfCommentBlocks();
		totalCommentLines += analyzer.getTotalLinesOfComments();
		if(analyzer.hasIndentation()) {
			filesWithIndentation ++;	
		}
	}
	
	public void printStatistics() {
		StringBuilder statistics = new StringBuilder("Analysis of files : \n");
		statistics.append("Number of files : " + numberOfFiles + "\n");
		statistics.append("Total Number of lines : " + totalNumberOfLines + "\n");
		statistics.append("Total Number of empty lines : " + totalNumberOfEmptyLines + "\n");
		statistics.append("Files with DOCTYPE declarations : " + filesWithDoctypeElements + "\n");
		statistics.append("Files with HEAD declarations : " + filesWithHeadElements + "\n");
		statistics.append("Files with TITLE declarations : " + filesWithTitleElements + "\n");
		statistics.append("Files with META declarations : " + filesWithMetaElements + "\n");
		statistics.append("Total number of tags : " + totalNumberOfTags + "\n");
		statistics.append("Total number of uppercase tags : " + totalUppercaseTags + "\n");
		statistics.append("Total number of lowercase tags : " + totalLowercaseTags + "\n");
		statistics.append("Total number of mixed case tags : " + totalMixedcaseTags + "\n");
		statistics.append("Total number of attributes : " + totalNumberOfAttributes + "\n");
		statistics.append("Total uppercase attributes : " + totalUppercaseAttributes + "\n");
		statistics.append("Total lowercase attributes : " + totalLowercaseAttributes + "\n");
		statistics.append("Total mixedcase attributes : " + totalMixedcaseAttributes + "\n");
		statistics.append("Total number of comment blocks (with one or multiple lines) : " + totalNumberOfCommentBlocks + "\n");
		statistics.append("Total lines of comments : " + totalCommentLines + "\n");
		statistics.append("Files with indentation : " + filesWithIndentation + "\n");
		System.out.println(statistics);
	}
	
}
