/*
 * This will be used when searching inside a directory and all its
 * contents. It defines what will happen each time we come across a 
 * file or a sub-directory. The purpose is to locate all the code 
 * files of a programming language, using the language extension 
 * (e.g. ".html"), depending on which language we are searching for. 
 */

package hdc.team_blue.filetree;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hdc.team_blue.language.Languages;
import hdc.team_blue.language.html.HTML2_Analyzer;
import hdc.team_blue.language.html.HTML2_Statistics;

public class FileSearcher implements FileVisitor<Path> {
	private Pattern fileExtension;              //Language extension (e.g ".html") in the form of a pattern
	private Languages language;                 //which language was used
	private Matcher matcher;
	private HTML2_Statistics html2_statistics;   //gathers statistics for html files
	
	public FileSearcher(Pattern languageExtension, Languages lang) {
		fileExtension = languageExtension;
		language = lang;
		html2_statistics = new HTML2_Statistics();
	}
	
	public HTML2_Statistics getHtml2_Statistics() {
		return html2_statistics;
	}
	
	public void setFileExtension(Pattern newExtension) {
		fileExtension = newExtension;
	}
	
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		matcher = fileExtension.matcher(file.getFileName().toString());
		if(matcher.find()) {
			//System.out.println(file);
			if(language == Languages.HTML) {
				HTML2_Analyzer analyzer = new HTML2_Analyzer();
				analyzer.parseFile(file);
				html2_statistics.updateStatistics(analyzer);
			}
		}
		return FileVisitResult.CONTINUE;
	}

	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

}
