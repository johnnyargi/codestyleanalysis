package hdc.team_blue.main;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import hdc.team_blue.filetree.FileSearcher;
import hdc.team_blue.language.CodeParser_Intf;
import hdc.team_blue.language.Languages;
import hdc.team_blue.language.html.HTML2_Analyzer;

public class StyleAnalyzer {
	private static Path searchPath;                      //the path of the file or the directory
	private static Languages languageOption;             //the selected language
	private static Pattern languageExtensionPattern;     //the file extension for the current language, as a pattern
	private static String errorMessage;                  //messages for the user
	private static StringBuilder usageMessage;
	private static FileSearcher fileSearcher;
	private static CodeParser_Intf codeAnalyzer;
	
	public static void main(String args[]) {
		//Keep track of time of execution
		long startTime = System.currentTimeMillis();
		//Initialize the user messages
		messageInitializer();
		//Check if all needed arguments are present
		if(args.length < 4) {
			System.out.println("Insufficient number of arguments. First, it needs a file or directory path, then a language option.");
			System.out.println(usageMessage);
			System.exit(1);
		}
		//Check argument validity and if correct proceed
		argumentProcessor(args);
		//Print total time of execution
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("Execution time : " + elapsedTime + " ms");
	}
	
	//Initializes any messages for the user
	private static void messageInitializer() {
		errorMessage = "Invalid arguments";
		usageMessage = new StringBuilder("Usage :\n");
		usageMessage.append("    -f <filename>        : check a file\n");
		usageMessage.append("    -d <directory name>  : search inside a directory\n");
		usageMessage.append("    -l <language>        : choose language\n");
		usageMessage.append("                           language options :\n");
		usageMessage.append("                            * html\n");
	}
	
	//Checks which language is selected and assigns appropriate regular expression
	//Returns false if the language is unsupported
	private static boolean languageChecker(String extension) {
		boolean supported = false;
		switch(extension) {
			case "html" :
				languageOption = Languages.HTML;
				languageExtensionPattern = Pattern.compile(".+?\\.html");
				codeAnalyzer = new HTML2_Analyzer();
				supported = true;
				break;
			default : 
				break;
		}
		return supported;
	}
	
	//Processes the arguments and if valid, it executes the file processing
	private static void argumentProcessor(String args[]) {
		switch(args[0]) {
			case "-f" :  //asks for a file search
				searchPath = Paths.get(args[1]);
				if (Files.exists(searchPath)) {    //check if the file exists
					if(args[2].equals("-l")) {
						if(languageChecker(args[3])) {   //check which language was requested
							codeAnalyzer.parseFile(searchPath);
							codeAnalyzer.printStatistics();
						}else {
							System.out.println("Unsupported language");
						}
					}else {
						System.out.println(errorMessage);
						System.out.println(usageMessage);
					}
				}else {
					System.out.println("The file does not exist.");
				}
				break;
			case "-d" :  //asks for a directory search
				searchPath = Paths.get(args[1]);
				if (Files.exists(searchPath) && (Files.isDirectory(searchPath))) {    //check if the directory exists
					if(args[2].equals("-l")) {
						if(languageChecker(args[3])) {    //check which language was requested
							fileSearcher = new FileSearcher(languageExtensionPattern, languageOption);
							try {
								Files.walkFileTree(searchPath, fileSearcher);
							} catch (IOException e) {
								System.err.println("Error while searching through the directory's contents : \n" + e.getMessage());
							}
							fileSearcher.getHtml2_Statistics().printStatistics();
						}else {
							System.out.println("Unsupported language");
						}
					}else {
						System.out.println(errorMessage);
						System.out.println(usageMessage);
					}
				}else {
					System.out.println("The directory does not exist.");
				}
				break;
			default : 
				System.out.println(errorMessage);
				System.out.println(usageMessage);
				break;
		}
	}

}
