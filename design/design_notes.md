##Design Notes


The documents will be opened and parsed line by line.

The parser will be a state machine that, if it supports more than one language, will have the following general requirements : 

1. Count the number of characters per line
2. Count empty lines (new line character only)
3. Understand whether it's reading code or a comment.
    * if it is a comment block it must count the lines
4. Understand whether it's inside a code block or outside. This will help keep track of the identation used to define the code block. For html the "block" is actually the tags and their content.
5. If it parses an expression, it must understand where the expression ends. That will help identify any spaces, tabs, new line characters used within the expression.
    * new line characters will give us multiple line expressions
6.  If it identifies variable declarations, it must check for the following :
    * camelCase
    * all capitals
    * all lowercase
    * delimeter_separated words (_, -)
7. Identify identation by using spaces or tabs before the beginning of text :
    * notice curly brace identation if inside code block
    * count number of spaces/tabs used for identation

-----------

HTML 2 specific requirements :

Requirements 1, 2, 3, 4 from the general ones and also :

* 6 for tags and  attributes - look for only uppercase or lowercase
* 7 without looking for curly braces
* search if the DOCTYPE declaration exists on top (rule)
* if any meta elements exist (suggestion)
* if the title element exists ( rule )

! HTML allows text outside of tags, care must be taken when parsing.  
Check for text outside tags
