##Code Style Analyzer

###Purpose

This tool was built to identify coding style features in old HTML 2 code from some backups, for a university course. The purpose is not to check correctness of the code, merely features like :

* Use or not of DOCTYPE, HEAD, TITLE and META elements
* Use or not of indentation 
* Uppercase or lowercase tags and attributes (or even mixed-case because it was detected in some old documents)
* Number of lines to compare to empty lines, comment blocks/lines

It can check the above for a single file or for all html files inside a folder and its subcontents. In either case it prints some statistics.

###Usage

The tool expects the following arguments by this order : 

1. -f for a file and its path or -d for a directory and its path
2. -l for the language option (for now just "html") 